#include <stdio.h>
float input()
{
    float c;
    
    printf("Enter the Number:");
    scanf("%f",&c);
    
    return c;
}
float find_vol(float c,float d,float e)
{
    float vol;
    vol=((c*d*e)+d/e)/3;
    
    
    return vol;
}
void output(float c,float d,float e,float vol)
{
    printf("Volume of tromboloid with h=%f,d=%f and b=%f is %f",c,d,e,vol);
    
}
int main()
{
    float h,d,b,v;
    
    h=input();
    d=input();
    b=input();
    v=find_vol(h,d,b);
    output(h,d,b,v);
    
    return 0;
}
